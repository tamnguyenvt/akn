/**
 * The main module that defines the public interface for akn,
 * a made-up library to demonstrate how to construct a source from components.
 */
define(function (require) {
    'use strict';

    // All dependencies (out of module) are required here.
    var $ = require('jquery');
    require('aknlib/helper');

    //Return the module value.
    return {
        version: '0.0.1, jQuery version is: ' + $.fn.jquery,
        instance: require('aknlib/bootstrap')
    };
});
