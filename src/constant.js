/**
 * Constant declaration
 * @package akn
 * @file constant.js
 */
define({
    ENV: { DEV: 'dev', PROD: 'prod'}
});