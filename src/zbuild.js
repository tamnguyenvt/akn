// Note this file must not be the first to be processed by uglify, or it causes an error
// because it requirejs is trying to read it while it is being uglified - hence the name
// zapp.build.js. Also moving this file to oump causes all folders within oump to be included in
// the uglification process (not wanted).
({
	appDir: ".", // Always start in the local/oump/src folder - all files are copied from here.
	baseUrl: ".",
	dir: "../build", // The folder for minified files.

	paths: {
		// Location of the app js files.
		aknlib: '.',

		// Core libraries.
		jquery: 'empty:',
	},

	modules: [
		{
			// The name of the final single minified file.
			name: "akn"
		}
	]
})