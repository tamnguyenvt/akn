/**
 * This file contains all helper functions that need for whole project.
 * @package akn
 * @file helper.js
 */

define([
], function () {

    /**
     * Show debug message in console
     * @method debug
     * @param string message Message text
     * @param int type Information type (optional)
     *  3: error
     *  2: warning
     *  1: info
     *  0: normal (default)
     */
    window.akndebug = window.akndebug || function (message, type) {
        switch (type) {
            case 3:
                console.error('AKN: ' + message);
                break;
            case 2:
                console.warn('AKN: ' + message);
                break;
            case 1:
                console.info('AKN: ' + message);
                break;
            case 0:
            default:
                console.log('AKN: ' + message);
                break;
        }
    };
});