/**
 * Bootstrap file that initializes module
 * @package akn
 * @file bootstrap.js
 */

define([
    'aknlib/box',
    'aknlib/constant'
], function (Box, CONST) {

    if (window.akn) {
        return window.akn;
    }

    /**
     * Configuration for module. It's secured.
     */
    var c = {
        env: CONST.ENV.PROD // prod: production, dev: development
    };

    /**
     * Identify module has initialized or not yet. Module is just allowed to install once.
     */
    var initialized = false;

    var boxes = {};

    var t = {

        /*
         * Initialize
         * Kick off event listeners and set up configuration
         * @method init
         * @param JSON conf Configuration for module
         */
        init: function (conf) {

            if (initialized) {
                window.akndebug('Initialized. Module is just allowed to install once.', 2);
                return;
            }

            initialized = true;

            // Override default configuration by the passed one
            for (var key in conf) {
                c[key] = conf[key];
            }

            // Notify akn environment.
            $('body').attr('data-akn-env', c.env);

            // Global events
            $(document).on({
                keydown: t.triggerBoxEvent.bind(t),
                keyup: t.triggerBoxEvent.bind(t),
                click: t.triggerBoxEvent.bind(t)
            });
        },

        /*
         * Trigger box event when Dom Event fired
         * @method triggerBoxEvent
         * @param Dom Event e
         */
        triggerBoxEvent: function (e) {
            var i;
            for (i in boxes) {
                boxes[i].triggerEvent(e);
            }
        },

        /*
         * Add new box instance
         * @method addBox
         * @param JSON options
         */
        addBox: function (options) {
            var container = $('#' + options.id)[0];

            if (!container) {
                window.akndebug('New instance was not created. No element found by given id "' + options.id + '"', 3);
                return;
            }

            options.autoinit = true;
            boxes[options.id] = new Box(options);
        },

        update: function (id) {
            if (boxes[id]) {
                boxes[id].update();
            } else{
                window.akndebug('Please provide correct box id. No box found by given id "' + id + '"', 3)
            }
        }
    };

    // Globalize akn module
    window.akn = t;

    return t;
});