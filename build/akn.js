/**
 * This file contains all helper functions that need for whole project.
 * @package akn
 * @file helper.js
 */

define('aknlib/helper',[
], function () {

    /**
     * Show debug message in console
     * @method debug
     * @param string message Message text
     * @param int type Information type (optional)
     *  3: error
     *  2: warning
     *  1: info
     *  0: normal (default)
     */
    window.akndebug = window.akndebug || function (message, type) {
        switch (type) {
            case 3:
                console.error('AKN: ' + message);
                break;
            case 2:
                console.warn('AKN: ' + message);
                break;
            case 1:
                console.info('AKN: ' + message);
                break;
            case 0:
            default:
                console.log('AKN: ' + message);
                break;
        }
    };
});
/**
 * Every Dom Element using tabindex are called Box. Everthing staying out of the box will use default
 * tabbing mechanism of browser.
 * @package akn
 * @file box.js
 */

define('aknlib/box',[],function () {
    var Box = function (options) {
        this.id = options.id;
        this.$el = $('#' + options.id);
        this.focusableEl = this.$entrypoint = this.$exitpoint = this.$lastpoint = $([]);

        options.autoinit && this.init();
    };

    /**
     * Initialize:
     *  - attach points
     *  - attach tabindex elements
     * @method init
     */
    Box.prototype.init = function () {
        var maxindex = 0;

        // Get max index in this box.
        this.$el.find('[data-akn-index]').each(function () {
            var tabindex = parseInt($(this).attr('data-akn-index'));
            if (tabindex > maxindex) {
                maxindex = tabindex;
            }
        });

        if (maxindex > 0) {
            // Insert points to this box.
            this.$el.before('<a href="#" tabindex="0" id="' + this.id + '-akn-enp">entry-point</a>');
            this.$el.after('<a href="#" tabindex="0" id="' + this.id + '-akn-exp">exit-point</a>');
            this.$el.append('<a href="#" data-akn-index="' + (maxindex + 1) + '" id="' + this.id +
                    '-akn-lp">last-point</a>');

            this.$entrypoint = $('#' + this.id + '-akn-enp');
            this.$exitpoint = $('#' + this.id + '-akn-exp');
            this.$lastpoint = $('#' + this.id + '-akn-lp');
        }

        this.focusableEl = this.$el.find('[data-akn-index]');

        // Start tabbing mode if tabbing mode is active currently.
        if (this.isActive()) {
            this.start();
        }
    };

    /**
     * Call when there are new tabindex elements added to the box.
     * So we need re-init the box.
     * @method update
     */
    Box.prototype.update = function () {
        this.$entrypoint.remove();
        this.$exitpoint.remove();
        this.$lastpoint.remove();

        this.init(); // Re-init.
    };

    /**
     * API to check if this box should be activated or deactivated once Dom Event fired.
     * @method triggerEvent
     * @param e DomEvent
     */
    Box.prototype.triggerEvent = function (e) {
        if (e.shiftKey && e.which == 16) { // Just press shift key only.
            return;
        }

        if (typeof this['handle' + e.type + e.which] != 'undefined') {
            this['handle' + e.type + e.which].call(this, e);
        }
    };

    /**
     * Deactivate tabbing on this box
     * @method stop
     */
    Box.prototype.stop = function () {
        this.$el.removeAttr('tabindex').attr('data-akn-state', 'unactive');
        this.focusableEl.each(function () {
            $(this).removeAttr('tabindex');
        });
    };

    /**
     * Activate tabbing on this box
     * @method start
     */
    Box.prototype.start = function () {
        this.$el.attr('data-akn-state', 'active');

        this.focusableEl.each(function () {
            $(this).attr('tabindex', $(this).attr('data-akn-index'));
        });
    };

    /**
     * Check if box is active.
     * @method start
     */
    Box.prototype.isActive = function () {
        return this.$el.attr('data-akn-state') == 'active';
    };

    /**
     * Handle key up event for tab key
     * @method handlekeyup9
     * @param e DomEvent
     */
    Box.prototype.handlekeyup9 = function (e) {
        var $target = $(e.target);
        var targetid = $target.attr('id');

        if (targetid == this.$entrypoint.attr('id')) {
            this.start();
            this.$el.attr('tabindex', 1).get(0).focus();
            return;
        }

        if (targetid == this.$lastpoint.attr('id')) {
            this.stop();
            this.$exitpoint.get(0).focus();
            return;
        }

        if (targetid == this.$exitpoint.attr('id') && e.shiftKey) {
            this.$el.attr('tabindex', 1).get(0).focus();
            this.start();
            return;
        }

        if ($(e.target).closest(this.$el).length > 0) {
            this.start();
        } else {
            this.stop();
        }
    };

    /**
     * Handle key down event for tab key
     * @method handlekeydown9
     * @param e DomEvent
     */
    Box.prototype.handlekeydown9 = function (e) {
        var $target = $(e.target);
        var targetid = $target.attr('id');
        
        if (targetid == this.$el.attr('id') && e.shiftKey) {
            this.stop();
            this.$entrypoint.attr('tabindex', -1);
            setTimeout(function () {
                this.$entrypoint.attr('tabindex', 0);
            }.bind(this), 300);
            return;
        }

        if (targetid == this.$exitpoint.attr('id') && e.shiftKey) {
            this.$el.attr('tabindex', 1).get(0).focus();
            this.start();
            e.preventDefault();
        }
    };

    /**
     * Handle click event
     * @method handlekeydown9
     * @param e DomEvent
     */
    Box.prototype.handleclick1 = function (e) {
        var $target = $(e.target);

        if ($target.closest(this.$el).length == 0) {
            this.stop();
        }
    };

    return Box;
});
/**
 * Constant declaration
 * @package akn
 * @file constant.js
 */
define('aknlib/constant',{
    ENV: { DEV: 'dev', PROD: 'prod'}
});
/**
 * Bootstrap file that initializes module
 * @package akn
 * @file bootstrap.js
 */

define('aknlib/bootstrap',[
    'aknlib/box',
    'aknlib/constant'
], function (Box, CONST) {

    if (window.akn) {
        return window.akn;
    }

    /**
     * Configuration for module. It's secured.
     */
    var c = {
        env: CONST.ENV.PROD // prod: production, dev: development
    };

    /**
     * Identify module has initialized or not yet. Module is just allowed to install once.
     */
    var initialized = false;

    var boxes = {};

    var t = {

        /*
         * Initialize
         * Kick off event listeners and set up configuration
         * @method init
         * @param JSON conf Configuration for module
         */
        init: function (conf) {

            if (initialized) {
                window.akndebug('Initialized. Module is just allowed to install once.', 2);
                return;
            }

            initialized = true;

            // Override default configuration by the passed one
            for (var key in conf) {
                c[key] = conf[key];
            }

            // Notify akn environment.
            $('body').attr('data-akn-env', c.env);

            // Global events
            $(document).on({
                keydown: t.triggerBoxEvent.bind(t),
                keyup: t.triggerBoxEvent.bind(t),
                click: t.triggerBoxEvent.bind(t)
            });
        },

        /*
         * Trigger box event when Dom Event fired
         * @method triggerBoxEvent
         * @param Dom Event e
         */
        triggerBoxEvent: function (e) {
            var i;
            for (i in boxes) {
                boxes[i].triggerEvent(e);
            }
        },

        /*
         * Add new box instance
         * @method addBox
         * @param JSON options
         */
        addBox: function (options) {
            var container = $('#' + options.id)[0];

            if (!container) {
                window.akndebug('New instance was not created. No element found by given id "' + options.id + '"', 3);
                return;
            }

            options.autoinit = true;
            boxes[options.id] = new Box(options);
        },

        update: function (id) {
            if (boxes[id]) {
                boxes[id].update();
            } else{
                window.akndebug('Please provide correct box id. No box found by given id "' + id + '"', 3)
            }
        }
    };

    // Globalize akn module
    window.akn = t;

    return t;
});
/**
 * The main module that defines the public interface for akn,
 * a made-up library to demonstrate how to construct a source from components.
 */
define('akn',['require','jquery','aknlib/helper','aknlib/bootstrap'],function (require) {
    'use strict';

    // All dependencies (out of module) are required here.
    var $ = require('jquery');
    require('aknlib/helper');

    //Return the module value.
    return {
        version: '0.0.1, jQuery version is: ' + $.fn.jquery,
        instance: require('aknlib/bootstrap')
    };
});

