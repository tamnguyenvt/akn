require.config({
    paths: {
        jquery: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery',
        'aknlib': '../src' // Run code in src folder
        //'akn': '../build/akn.min' // Run code in build folder
        //'akn': '../build/akn' // to run code in build folder without optimizing
    }
});
require(['aknlib/akn', 'jquery'], function(akn) { // Run code in src folder
//require(['akn', 'jquery'], function(akn, $) { // Run code in build folder

    console.log(akn.version);

    // Init tabbing feature.
    var aknins = akn.instance;
    aknins.init({
        env: 'prod'
    });

    // Create instances
    aknins.addBox({
        id: 'container-1'
    });

    aknins.addBox({
        id: 'container-2'
    });

    aknins.addBox({
        id: 'container-3'
    });

    // Update box
    var index = 1;
    $('#addboxitem').click(function () {
        index ++;
        $('#container-3')
            .find('div.row')
            .append(
                '<div href="#" class="div' + index + '" data-akn-index="' + index + '">' + index + '</div> <br>')
            .find('.div' + index)
            .css({
                top: Math.floor(Math.random() * 170) + 1   + 'px',
                right: Math.floor(Math.random() * 400) + 30 + 'px',
                'background-color': '#D9853B'
            });

        aknins.update('container-3');
    });
});